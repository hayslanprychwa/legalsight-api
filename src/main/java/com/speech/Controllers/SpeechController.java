package com.speech.Controllers;


import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.speech.Entities.Speech;
import com.speech.Repositories.SpeechRepository;

@RestController
@RequestMapping("/speech")
public class SpeechController {
	
	@Autowired
	private SpeechRepository speechRepository;

		@RequestMapping(value="/findAll", method=RequestMethod.GET)
		public Iterable<Speech> getAllSpeech() {	
		return speechRepository.findAll();
		}
		
		@RequestMapping(value="/findById/{id}", method=RequestMethod.GET)
		public Optional<Speech> getSpeechById(@PathVariable Integer id) {	
		return speechRepository.findById(id);
		}
		
		@RequestMapping(value="/findByTitle/{title}", method=RequestMethod.GET)
		public Optional<Speech> getSpeechByTitle(@PathVariable String title) {	
		return speechRepository.findByTitle(title);
		}
		
		@RequestMapping(value="/findByKeyword/{keyword}", method=RequestMethod.GET)
		public Optional<Speech> getSpeechByKeyword(@PathVariable String keyword) {	
		return speechRepository.findByKeyword(keyword);
		}
		
		@RequestMapping(value="/findByDatetime/{datestart}/{dateend}", method=RequestMethod.GET)
		public List<Speech> getSpeechByDatetime(@PathVariable("datestart") String datestart, @PathVariable("dateend") String dateend) {	
		return speechRepository.findByDatetime(datestart, dateend);
		}
		
		@RequestMapping(value ="/create", method=RequestMethod.POST)
		public @ResponseBody
		Iterable<Speech> addSpeech(@RequestBody Speech jsonString) {
		speechRepository.save(jsonString);
		return getAllSpeech();
		}
		
		@RequestMapping(value ="/update", method=RequestMethod.PUT)
		public @ResponseBody
		Iterable<Speech> updateSpeech(@RequestBody Speech jsonString) {
		speechRepository.saveAndFlush(jsonString);
		return getAllSpeech();
		}
		
		@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
		public Iterable<Speech> deleteSpeech(@PathVariable Integer id) {	
		speechRepository.deleteById(id);
		return getAllSpeech();
		}
}
