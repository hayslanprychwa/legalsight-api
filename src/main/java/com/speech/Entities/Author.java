package com.speech.Entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="AUTHOR")
@TableGenerator(name="tab", initialValue=0, allocationSize=50)
public class Author {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="tab")
	Integer author_id;
	String name;
	Integer speech_id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="speech_id", insertable=false, updatable=false, nullable = false)    
	private Speech speech;

	public Author() {

	}

	public Author(Integer author_id, String name, Integer speech_id) {
		super();
		this.author_id = author_id;
		this.name = name;
		this.speech_id = speech_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSpeech_id() {
		return speech_id;
	}

	public void setSpeech_id(Integer speech_id) {
		this.speech_id = speech_id;
	}
}
