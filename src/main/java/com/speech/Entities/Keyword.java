package com.speech.Entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="KEYWORD")
@TableGenerator(name="tab", initialValue=0, allocationSize=50)
public class Keyword {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="tab")
	Integer keyword_id;
	String keyword;
	Integer speech_id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="speech_id", insertable=false, updatable=false, nullable = false)    
	private Speech speech;
	
	public Keyword() {

	}

	public Keyword(Integer keyword_id, String keyword, Integer speech_id) {
		super();
		this.keyword_id = keyword_id;
		this.keyword = keyword;
		this.speech_id = speech_id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getSpeech_id() {
		return speech_id;
	}

	public void setSpeech_id(Integer speech_id) {
		this.speech_id = speech_id;
	}
}
