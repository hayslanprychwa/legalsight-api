package com.speech.Entities;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime; 

@Entity
@Table(name="SPEECH")
public class Speech {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer speech_id;
	String title;
	@Column(columnDefinition="LONGTEXT")
	String speech;
	LocalDateTime date;
	
	@OneToMany(mappedBy = "speech", fetch = FetchType.LAZY, cascade =
	CascadeType.ALL) private Set<Author> author;
	 
	
	@OneToMany(mappedBy = "speech", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Keyword> keyword;
	
	public Speech() {
	
	}
	
	public Speech(Integer speech_id, String title, String speech, LocalDateTime date) {
		this.speech_id = speech_id;
		this.title = title;
		this.speech = speech;
		this.date = date;
	}

	public Integer getSpeech_id() {
		return speech_id;
	}

	public void setSpeech_id(Integer speech_id) {
		this.speech_id = speech_id;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSpeech() {
		return speech;
	}

	public void setSpeech(String speech) {
		this.speech = speech;
	}
	
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Set<Author> getAuthor() { 
		return author; }
	  
	public void setAuthor(Set<Author> author) { 
		this.author = author; }
	 
	public Set<Keyword> getKeyword() {
		return keyword;
	}

	public void setKeyword(Set<Keyword> keyword) {
		this.keyword = keyword;
	}
	
}
