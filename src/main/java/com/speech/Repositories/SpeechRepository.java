package com.speech.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.speech.Entities.Speech;

import java.util.List;
import java.util.Optional;

public interface SpeechRepository extends JpaRepository<Speech, Integer>{

	@Query(value = "SELECT * FROM SPEECH s WHERE s.TITLE LIKE %:title%", nativeQuery = true)
	Optional<Speech> findByTitle(String title);
	
	@Query(value = "SELECT * FROM SPEECH sp INNER JOIN KEYWORD kw ON kw.SPEECH_ID=sp.SPEECH_ID WHERE kw.KEYWORD LIKE %:keyword%", nativeQuery = true)
	Optional<Speech> findByKeyword(String keyword);

	@Query(value = "SELECT * FROM SPEECH s WHERE s.DATE BETWEEN :datestart AND :dateend", nativeQuery = true)
	List<Speech> findByDatetime(String datestart, String dateend);

}
